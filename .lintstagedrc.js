module.exports = {
  '*.{ts,tsx,js,jsx,json,scss,css,md}': [`prettier --write`, `git add`],
}
