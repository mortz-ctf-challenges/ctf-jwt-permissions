import axios, { AxiosRequestConfig } from 'axios'
import fastify from 'fastify'
import fastifyJwt from 'fastify-jwt'
import fastifyCors from 'fastify-cors'

const server = fastify({ logger: true })

if (!String(process.env.PORT).match(/^\d+$/)) {
  console.error('Required env "PORT" must be defined as a number')
  process.exit(1)
}
if (process.env.AUTH_SERVICE_URL == null) {
  console.error('Required env "AUTH_SERVICE_URL" must be defined')
  process.exit(1)
}
if (process.env.CTF_FLAG == null) {
  console.error('Required env "CTF_FLAG" must be defined')
  process.exit(1)
}

server.register(fastifyCors)

server.get('/flag', async (req, res) => {
  let tokenPayload: { username: string; isAdmin: boolean }

  try {
    tokenPayload = await req.jwtVerify()
  } catch (err) {
    res.status(401)
    return res.send({ message: 'Unable to verify bearer token' })
  }

  if (!tokenPayload.isAdmin) {
    res.status(403)
    return res.send({ message: "You don't have sufficient rights to order this flag" })
  } else {
    return res.send({ flag: process.env.CTF_FLAG })
  }
})

server.get('/zd', async (_req, res) => {
  return res.send(true)
})

const start = async () => {
  const jwtPublicKey = String(
    (await axios(<AxiosRequestConfig>{
      method: 'GET',
      url: `${String(process.env.AUTH_SERVICE_URL).replace(/\/*$/, '')}/public/public.pem`,
    })).data
  )
  if (!String(jwtPublicKey).includes('BEGIN PUBLIC KEY')) {
    console.error(
      'Could not fetch the private key from the auth service. Check the AUTH_SERVICE_URL env var, and try again.'
    )
    return null
  }
  server.register(fastifyJwt, {
    secret: {
      public: jwtPublicKey,
      private: 'unused',
    },
  })

  try {
    await server.listen({
      host: '0.0.0.0',
      port: Number(process.env.PORT),
    })
    console.info(`server listening at http://localhost:${process.env.PORT}`)
  } catch (err) {
    server.log.error(err)
    process.exit(1)
  }
}
start()
