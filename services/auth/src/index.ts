import * as path from 'path'
import * as fs from 'fs'
import fastify from 'fastify'
import fastifyStatic from 'fastify-static'
import fastifyFormbody from 'fastify-formbody'
import fastifyJwt from 'fastify-jwt'
import fastifyCors from 'fastify-cors'
import { RateLimiterMemory } from 'rate-limiter-flexible'

// Data
import dataUsers from './data/users'

const server = fastify({ logger: true })

if (process.env.PORT == null) {
  console.error('Required env "PORT" is undefined')
  process.exit(1)
}
if (!(<string>process.env.PORT).match(/^\d+$/i)) {
  console.error('Required env "PORT" must be a number')
}

server.register(fastifyStatic, {
  root: path.resolve(__dirname, '../jwt-keys'),
  prefix: '/public/',
})
server.register(fastifyJwt, {
  sign: {
    algorithm: 'ES512',
    expiresIn: 60 * 60,
  },
  secret: {
    public: fs.readFileSync(path.resolve(__dirname, '../jwt-keys/public.pem')).toString(),
    private: fs.readFileSync(path.resolve(__dirname, '../jwt-keys/private.pem')).toString(),
  },
})
server.register(fastifyFormbody)
server.register(fastifyCors)

const failedLoginRateLimit = new RateLimiterMemory({
  points: 5, // Five attempts
  duration: 60, // Pr. min.
})
server.post('/login', async (req, res) => {
  const username: string = req.body && req.body.username
  const password: string = req.body && req.body.password

  if (!username || !password) {
    res.status(400)
    return res.send({ message: 'Form body params "username" and "password" is required' })
  }

  const rateLimitId: string = `${req.ip}_${username}`

  const rateLimitState = await failedLoginRateLimit.get(rateLimitId)
  if (rateLimitState != null && rateLimitState.remainingPoints <= 0) {
    res.status(429)
    return res.send({ message: 'Too many failed login attempts, wait a minute before trying again' })
  }

  const userInstance = dataUsers.find((user) => user.username.toLowerCase() === username.toLowerCase())
  if (userInstance == null) {
    res.status(404)
    return res.send({ message: 'Username not found' })
  }
  if (userInstance.password === password) {
    return res.send({
      token: await server.jwt.sign({
        username: userInstance.username,
        canOrderSpecialFlags: userInstance.canOrderSpecialFlags,
      }),
    })
  } else {
    failedLoginRateLimit.consume(rateLimitId, 1)
    res.status(401)
    return res.send({ message: 'Wrong password' })
  }
})

server.get('/zd', async (_req, res) => {
  return res.send(true)
})

const start = async () => {
  try {
    await server.listen({
      host: '0.0.0.0',
      port: Number(process.env.PORT),
    })
    console.info(`server listening at http://localhost:${process.env.PORT}`)
  } catch (err) {
    server.log.error(err)
    process.exit(1)
  }
}
start()
