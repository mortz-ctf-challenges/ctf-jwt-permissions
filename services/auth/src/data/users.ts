export type UserData = {
  username: string
  password: string
  canOrderSpecialFlags: boolean
}

export default [
  {
    username: 'Admin',
    password: Buffer.from('No one is really meant to guess this password').toString('base64'),
    canOrderSpecialFlags: true,
  },
  {
    username: 'Ding',
    password: 'dong',
    canOrderSpecialFlags: false,
  },
] as Array<UserData>
