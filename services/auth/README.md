# Service - Auth

This service works as the auth server, it handles all operations related to authentication.

## Setup

In order for this service to work, a ES512 key pair will have to be provided in `./jwt-keys`. To generate those keys, you can utilise the following commands:

```bash
# Private
openssl ecparam -genkey -name secp521r1 -noout -out ./jwt-keys/private.pem

# Public
openssl ec -in ./jwt-keys/private.pem -pubout -out ./jwt-keys/public.pem
```

Alternatively, if you have npm or yarn available, you can simple use the helper to generate the keys with the following command:

```bash
# NPM
npm run generate-jwt-keys

# Yarn
yarn generate-jwt-keys
```

## Environment

Below is a table of the environment variables that the app uses

| Name | Required | Description                    |
| ---- | -------- | ------------------------------ |
| PORT | Yes      | Port for the service to run on |
