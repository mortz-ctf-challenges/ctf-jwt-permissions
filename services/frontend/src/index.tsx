import * as React from 'react'
import * as ReactDOM from 'react-dom'

// Components
import { App } from './App'

ReactDOM.render(<App />, document.getElementById('AppRoot'))
