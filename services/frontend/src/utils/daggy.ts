// @ts-ignore
import { taggedSum } from 'daggy'

export type RemoteDataType = {
  NotAsked: any
  Loading: any
  Error: (error: Error) => any
  Success: (data: any) => any
}
export const RemoteData: RemoteDataType = taggedSum('RemoteData', {
  NotAsked: [],
  Loading: [],
  Error: ['error'],
  Success: ['data'],
})
