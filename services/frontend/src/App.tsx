import * as React from 'react'

// Components
import { Login } from './components/login/index'
import { GetFlag } from './components/getFlag/index'

// Utils
import { RemoteData } from './utils/daggy'

// Styles
import './App.scss'

type AuthContextType = {
  bearerToken: any
  updateToken: (data: object) => void
}
const AuthContext = React.createContext({} as AuthContextType)

type AppState = {
  bearerToken: any
}
class App extends React.Component<{}, AppState> {
  state = {
    bearerToken: RemoteData.NotAsked,
  }

  render() {
    return (
      <div id="App">
        <AuthContext.Provider
          value={
            {
              bearerToken: this.state.bearerToken,
              updateToken: (data) => this.setState({ bearerToken: data }),
            } as AuthContextType
          }
        >
          <Login />
          <GetFlag />
        </AuthContext.Provider>
      </div>
    )
  }
}

export { App, AuthContext, AuthContextType }
