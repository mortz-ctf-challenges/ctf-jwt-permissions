import * as React from 'react'
import axios from 'axios'
import { RemoteData } from '../../utils/daggy'

// Styles
import './GetFlag.scss'

class GetFlag extends React.Component {
  state = {
    token:
      'eyJhbGciOiJFUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IkRpbmciLCJjYW5PcmRlclNwZWNpYWxGbGFncyI6ZmFsc2UsImlhdCI6MTU2NDMxNTU5OCwiZXhwIjoxNTY0MzE5MTk4fQ.ATwTYDHeTHRtKMuc3R83KgK6J7IOlyUp3EfF1Als8k0Z6GPD3OvjczjaCwL9W8AgSWlgMi2fZPRhw8u6dhPbRT22AfeurA-QeIkPlLbaYoNkvhduQpnj2G63GzNy5wEh-KLwQ6nrx4Im6UViP78fAW80fLETFSJVgjBuO2zSzNn8JjvP',
    flag: RemoteData.NotAsked,
  }

  handleChangeInput = (name: string) => (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const value = e.target.value

    this.setState({
      [name]: value,
    })
  }

  handleSubmit = (e: React.FormEvent | React.MouseEvent) => {
    e.preventDefault()
    e.stopPropagation()

    this.setState({ flag: RemoteData.Loading })

    axios({
      method: 'GET',
      url: `${process.env.FLAG_SERVICE_URL}/flag`,
      headers: {
        authorization: `Bearer ${this.state.token}`,
      },
    }).then(
      (response) => {
        this.setState({ flag: RemoteData.Success(response.data) })
      },
      (error) => {
        this.setState({ flag: RemoteData.Error((error.response && error.response.data) || error) })
      }
    )
  }

  render() {
    return (
      <div id="GetFlag">
        <h1>Order special flag here!</h1>
        <form action="post" id="GetFlagForm" onSubmit={this.handleSubmit}>
          <textarea
            id="GetFlagToken"
            name="token"
            value={this.state.token}
            placeholder="Voucher"
            onChange={this.handleChangeInput('token')}
          />
          <input
            type="submit"
            name="submit"
            id="GetFlagSubmit"
            value={RemoteData.Loading.is(this.state.flag) ? 'Ordering flag...' : 'Submit'}
            disabled={RemoteData.Loading.is(this.state.flag)}
            onClick={this.handleSubmit}
          />
        </form>
        {this.state.flag.cata({
          NotAsked: () => null,
          Loading: () => null,
          Error: (error: Error) => (
            <div id="GetFlagError" className="error-text">
              {error.message}
            </div>
          ),
          Success: (data: any) => (
            <>
              Here is your brand new flag!
              <div id="GetFlagFlag">{data.flag}</div>
            </>
          ),
        })}
      </div>
    )
  }
}

export { GetFlag }
