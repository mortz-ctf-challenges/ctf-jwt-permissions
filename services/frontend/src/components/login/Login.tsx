import * as React from 'react'
import axios from 'axios'
import { RemoteData } from '../../utils/daggy'

// Components
import { AuthContext } from '../../App'

// Styles
import './Login.scss'

class Login extends React.Component {
  state = {
    username: '',
    password: '',
  }

  handleChangeInput = (name: string) => (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      [name]: e.target.value,
    })
  }

  handleSubmit = (updateToken: (data: object) => void) => (e: React.FormEvent | React.MouseEvent) => {
    e.preventDefault()
    e.stopPropagation()

    updateToken(RemoteData.Loading)

    axios({
      method: 'POST',
      url: `${process.env.AUTH_SERVICE_URL}/login`,
      data: {
        username: this.state.username,
        password: this.state.password,
      },
    }).then(
      (response) => {
        updateToken(RemoteData.Success(response.data))
      },
      (error) => {
        updateToken(RemoteData.Error(error.response.data))
      }
    )
  }

  render() {
    return (
      <div id="Login">
        <h1>Please login to get your voucher</h1>
        <AuthContext.Consumer>
          {(value) => (
            <>
              <form action="post" id="LoginForm" onSubmit={this.handleSubmit(value.updateToken)}>
                <input
                  type="text"
                  name="username"
                  id="LoginUsername"
                  value={this.state.username}
                  placeholder="Username"
                  onChange={this.handleChangeInput('username')}
                  required
                />
                <input
                  type="password"
                  name="password"
                  id="LoginPassword"
                  value={this.state.password}
                  placeholder="Password"
                  onChange={this.handleChangeInput('password')}
                  required
                />
                <input
                  type="submit"
                  name="submit"
                  id="LoginSubmit"
                  value={RemoteData.Loading.is(value.bearerToken) ? 'Logging in...' : 'Submit'}
                  disabled={RemoteData.Loading.is(value.bearerToken)}
                  onClick={this.handleSubmit(value.updateToken)}
                />
              </form>
              {value.bearerToken.cata({
                NotAsked: () => null,
                Loading: () => null,
                Error: (error: Error) => (
                  <div id="LoginError" className="error-text">
                    {error.message}
                  </div>
                ),
                Success: (data: any) => (
                  <>
                    Almost there! Carry the following token to the next step in the order process
                    <div id="LoginBearerToken">{data.token}</div>
                  </>
                ),
              })}
            </>
          )}
        </AuthContext.Consumer>
      </div>
    )
  }
}

export { Login }
