# Service - Auth

This service works as the flag server, it checks if the user is admin by use of supplied jwt, and supplies flag if it is.

## Environment

Below is a table of the environment variables that the app uses

| Name             | Required | Description                                              |
| ---------------- | -------- | -------------------------------------------------------- |
| PORT             | Yes      | Port for the service to run on                           |
| AUTH_SERVICE_URL | Yes      | Url pointing to the auth service entry point             |
| CTF_FLAG         | Yes      | Flag the service should supply upon challenge completion |
